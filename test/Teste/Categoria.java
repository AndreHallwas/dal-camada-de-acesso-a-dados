/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Teste;

import Dal.Entidade.ControledeEntidade;
import Dal.Indice.IndiceDal;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aluno
 */
public final class Categoria extends ControledeEntidade {

    private String Codigo;
    private String Nome;
    private String Descricao;

    public Categoria(String Codigo, String Nome, String Descricao) {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Descricao = Descricao;
    }

    public Categoria() {
    }

    public String getCodigo() {
        return Codigo;
    }

    public String getNome() {
        return Nome;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo + "";
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    @Override
    protected void getIndicesEmpty() {
        Indices.add(new IndiceDal(Codigo, "cat_cod", "int", '1', "Código", "Codigo"));
        Indices.add(new IndiceDal(Nome, "cat_nome", "str", "Nome"));
        Indices.add(new IndiceDal(Descricao, "cat_descricao", "str", "Descrição", "Descricao"));
    }

    public Field[] getFields() {
        return getClass().getDeclaredFields();
    }

    Object getFieldValue(Categoria c, int i) {
        try {
            return getFields()[i].get(c);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void setTabela() {
        Tabela = "Categoria";
    }

}
