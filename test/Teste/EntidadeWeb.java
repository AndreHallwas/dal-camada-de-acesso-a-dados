/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Teste;

import Dal.Indice.IndiceDal;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class EntidadeWeb {

    protected ArrayList<IndiceDal> Indices;

    public abstract ArrayList<IndiceDal> getIndices();

    public String toHiperLineTableHeader(IndiceDal indice) {
        return "<th>" + indice.getNome() + "</th>";
    }
    
    public String toHiperLineTableLine(IndiceDal indice) {
        return "<td>" + ((indice.getAtributo() + "").isEmpty() ? "" : indice.getAtributo() + "") + "</td>";
    }

    public String toHiperLine(int Parametro, String... param) {
        String string = "";
        String string2 = "";
        Indices = getIndices();
        IndiceDal chave = new IndiceDal("nada", "nada", "nada", '1', "nada");
        string2 += "<tbody><tr>";
        if (Parametro == 0) {
            string += "<thead><tr>";
            for (IndiceDal indice : Indices) {
                string += toHiperLineTableHeader(indice);
                string2 += toHiperLineTableLine(indice);
                if (indice.getChave() == 1) {
                    chave = indice;
                }
            }
            string += "<th class='actions'>Ações</th>";
            string += "</tr></thead>";
        } else {
            for (IndiceDal indice : Indices) {
                string2 += toHiperLineTableLine(indice);
                if (indice.getChave() == 1) {
                    chave = indice;
                }
            }
        }
        string2 += toHiperLineActions(chave,param);
        string2 += "</tr></tbody>";
        return string + string2;

    }
    
    public String toHiperLineActions(IndiceDal chave,String... Param){
        String string = "";
        string += "<td class='actions'>";
        string += toHiperLineButtonCRUD(chave, Param);   
        string += "<input type='hidden' id='chave' value='" + ((chave.getAtributo() + "").isEmpty() ? "" : chave.getAtributo() + "") + "'/>";
        string += "</td>";
        return string;
    }
    
    public String toHiperLineButtonCRUD(IndiceDal chave, String... Param){
        String string = "";
        if(Param.length>0 && Param[0].equals("1")){
             string += "<a class='btn btn-success btn-xs' href='CtrlInterface?acao=painelCentralView&tipo=usuario&param=visualizar&chave=" + chave.getAtributo().toString() + "'>Visualizar</a>"
                + "<a class='btn btn-warning btn-xs' href='CtrlInterface?acao=painelCampos&tipo=usuario&param=alterar&chave=" + chave.getAtributo().toString() + "'>Editar</a>"
                + "<a class='btn btn-danger btn-xs' data-toggle='modal' data-target='#delete-modal'>Excluir</a>";
        }else{
             string += "<a class='btn btn-success btn-xs' href='CtrlInterface?acao=painelCentralView&tipo=usuario&param=visualizar&chave=" + chave.getAtributo().toString() + "'>Visualizar</a>"
                + "<a class='btn btn-warning btn-xs' href='CtrlInterface?acao=painelCampos&tipo=usuario&param=alterar&chave=" + chave.getAtributo().toString() + "'>Editar</a>"
                + "<a class='btn btn-danger btn-xs' data-toggle='modal' data-target='#delete-modal'>Excluir</a>";
        }
       
        return string;
    }

    public String toHiperLineColumn(String Campo, int ColType, String Tipo) {
        return "<div class='form-group col-md-" + ColType + "'><label for='" + Campo + "'>" + Campo + "</label>"
                + "<input type='" + Tipo + "' class='form-control' name='" + Campo + "' placeholder='Digite o valor'></div>";
    }

    public String toHiperLineColumn(String Campo, int ColType, String Value, String Tipo) {
        return "<div class='form-group col-md-" + ColType + "'><label for='" + Campo + "'>" + Campo + "</label>"
                + "<input type='" + Tipo + "' class='form-control' name='" + Campo + "' value='" + Value + "' placeholder='Digite o valor'></div>";
    }

    public String toHiperLineColumns(int chave) {
        String string = "<div class='row'>";
        ArrayList<IndiceDal> Indices = getIndices();

        if (chave == 1) {
            for (IndiceDal indice : Indices) {
                string += toHiperLineColumn(indice.getNome(), 4, "text");
            }
        } else {
            for (IndiceDal indice : Indices) {
                string += toHiperLineColumn(indice.getNome(), 4, (indice.getAtributo() == null ? ""
                        : indice.getAtributo().toString()), "text");
            }
        }

        string += "</div>";
        return string;
    }

    public String toHiperLineViewColumn(String Campo, String Informacao) {
        return "<div class='col-md-4'>" + "<p><strong>" + Campo + "</strong></p>"
                + "<p>" + Informacao + "</p>" + "</div>";
    }

    public String toHiperLineViewColumns() {
        String string = "<div class='row'>";
        ArrayList<IndiceDal> Indices = getIndices();
        string += "<h3 class='page-header'>" + "Usuario" + "</h3>"
                + "<div class='row'>";
        for (IndiceDal indice : Indices) {
            string += toHiperLineViewColumn(indice.getNome(), (indice.getAtributo() == null ? ""
                    : indice.getAtributo().toString()));
        }
        string += "</div>";
        return string;
    }
}
