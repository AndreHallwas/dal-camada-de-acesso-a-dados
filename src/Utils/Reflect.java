/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.lang.reflect.Method;
import java.time.LocalDate;

/**
 *
 * @author Raizen
 */
public class Reflect {

    public static Object getObject(String nomeClasse, Object obj, String Metodo, int flag, Object... args) {

        try {
            Class cls = Class.forName(nomeClasse);
            obj = cls.newInstance();
            if (flag == 1) {
                return obj;
            }
        } catch (Exception e) {
            Mensagem.ExibirException(e);
        }

        Class[] params = {};
        if (args.length > 0) {
            params = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof Integer) {
                    params[i] = Integer.TYPE;
                } else if (args[i] instanceof Character) {
                    params[i] = Character.TYPE;
                } else if (args[i] instanceof Double) {
                    params[i] = Double.TYPE;
                } else if (args[i] instanceof Float) {
                    params[i] = Float.TYPE;
                } else if (args[i] instanceof Long) {
                    params[i] = Long.TYPE;
                } else if (args[i] == null) {
                    return true;
                } else if (args[i] instanceof java.sql.Date) {
                    params[i] = LocalDate.class;
                } else {
                    params[i] = args[i].getClass();/*Integer.TYPE*/
                }
            }
        }
        try {
            /////obj.getClass().getMethods()
            Method method = obj.getClass().getMethod(Metodo, params);
            method.invoke(obj, args);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }
}
