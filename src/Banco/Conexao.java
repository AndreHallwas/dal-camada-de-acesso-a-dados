/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

/**
 *
 * @author Raizen
 */
import Utils.Mensagem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Conexao {

    private Connection con;
    private String erro;
    private static String Driver;
    private String local;
    private String banco;
    private String usuario;
    private String senha;
    private static boolean OnNotManual = true;
    private static boolean OpenConection = false;

    protected abstract void ConexaoEspecifica(String local, String banco, String usuario,
            String senha, String Driver, Object... Params) throws ClassNotFoundException, SQLException;

    protected Conexao(String... Params) {
        erro = "";
        con = null;
    }

    /**
     * Params [0] == force Connection;
     *
     * @param local
     * @param banco
     * @param usuario
     * @param senha
     * @param Params
     * @return
     */
    public boolean conectar(String local, String banco, String usuario, String senha, Object... Params) {
        boolean conectado = true;
        try {
            if (con == null || con.isClosed()
                    || (Params != null && Params.length > 0 && Params[0] instanceof Boolean && (Boolean) Params[0] == true)) {
                this.setLocal(local);
                this.setBanco(banco);
                this.setUsuario(usuario);
                this.setSenha(senha);
                ConexaoEspecifica(local, banco, usuario, senha, getDriver(), Params);
            }
        } catch (SQLException sqlex) {
            setErro("Impossivel conectar com a base de dados: " + sqlex.toString());
            Mensagem.ExibirException(sqlex);
            conectado = false;
        } catch (ClassNotFoundException exclass) {
            setErro("Não foi Possivel Encontrar o Driver " + exclass.toString());
            Mensagem.ExibirException(exclass);
            conectado = false;
        }
        return OpenConection = conectado;
    }

    protected boolean reconectar() {
        boolean conectado = true;
        try {
            ConexaoEspecifica(getLocal(), getBanco(), getUsuario(), getSenha(), getDriver());
        } catch (SQLException sqlex) {
            setErro("Impossivel conectar com a base de dados: " + sqlex.toString());
            Mensagem.ExibirException(sqlex);
            conectado = false;
        } catch (ClassNotFoundException exclass) {
            setErro("Não foi Possivel Encontrar o Driver " + exclass.toString());
            Mensagem.ExibirException(exclass);
            conectado = false;
        }
        return OpenConection = conectado;
    }

    public boolean manipular(String sql) // inserir, alterar,excluir
    {
        boolean executou = false;
        try {
            Statement statement = getCon().createStatement();
            int result = statement.executeUpdate(sql);
            statement.close();
            if (result >= 1) {
                executou = true;
            }
        } catch (SQLException sqlex) {
            Mensagem.ExibirException(sqlex);
            setErro("Erro: " + sqlex.toString());
        }
        return executou;
    }

    public boolean manipular(PreparedStatement pmst) // inserir, alterar,excluir
    {
        boolean executou = false;
        try {
            int result = pmst.executeUpdate();
            pmst.close();
            if (result >= 1) {
                executou = true;
            }
        } catch (SQLException sqlex) {
            Mensagem.ExibirException(sqlex);
            setErro("Erro: " + sqlex.toString());
        }
        return executou;
    }

    public ResultSet ConsultarFunction(String sql, Object... Params) {
        PreparedStatement pstmt = geraStatement(sql);
        try {
            if (pstmt != null) {
                if (Params != null && Params.length > 0) {
                    for (int i = 1; i <= Params.length; i++) {
                        Object Param = Params[i-1];
                        pstmt.setObject(i, Param);
                    }
                    pstmt.execute();
                    return pstmt.getResultSet();
                } else {
                    pstmt.execute();
                }
            }
        } catch (SQLException ex) {
            Mensagem.ExibirException(ex, "Erro no Login");
        }
        return null;
    }

    public ResultSet consultar(String sql) {
        ResultSet rs = null;
        try {
            inicializaConsulta();
            Statement statement = getCon().createStatement();
            //ResultSet.TYPE_SCROLL_INSENSITIVE,
            //ResultSet.CONCUR_READ_ONLY);
            rs = statement.executeQuery(sql);
            //statement.close();
        } catch (SQLException sqlex) {
            Mensagem.ExibirException(sqlex);
            setErro("Erro: " + sqlex.toString());
            rs = null;
        }
        return rs;
    }

    public ResultSet consultar(PreparedStatement statement) {
        ResultSet rs = null;
        try {
            inicializaConsulta();
            rs = statement.executeQuery();
        } catch (SQLException sqlex) {
            Mensagem.ExibirException(sqlex);
            setErro("Erro: " + sqlex.toString());
        }
        return rs;
    }

    public void inicializaConsulta() throws SQLException {
        /*if (con.getAutoCommit() == false) {
            reverterTransacao();
            Mensagem.ExibirLog("Problema na consulta autocommit off foi resolvido");
        }*/
    }

    public PreparedStatement geraStatement(String Sql) {
        try {
            return getCon().prepareStatement(Sql);
        } catch (SQLException sqlex) {
            Mensagem.ExibirException(sqlex);
            setErro("Erro: " + sqlex.toString());
        }
        return null;
    }

    /**
     * @param Params Param[0] is on not Manual set false to manual transaction,
     * and set true to manual transaction
     * @return
     */
    public boolean iniciarTransacao(Object... Params) {
        try {
            if (Params != null && Params.length > 0) {
                setOnNotManual((boolean) Params[0]);
            }
            if (OpenConection == false || getCon() == null || getCon().isClosed()) {
                reconectar();
            }
            if (OpenConection == true) {
                getCon().setAutoCommit(false);
            } else {
                return false;
            }
        } catch (SQLException ex) {
            Mensagem.ExibirException(ex, "Erro no commit do banco ao Iniciar Transação");
            reverterTransacao();
            return false;
        }
        return true;
    }

    /**
     * @param Params Param[0] is on not Manual set false to manual transaction,
     * and set true to manual transaction
     * @return
     */
    public boolean finalizarTransacao(Object... Params) {
        try {
            if (Params != null && Params.length > 0) {
                setOnNotManual((boolean) Params[0]);
            }
            if (isOnNotManual()) {
                getCon().commit();
                getCon().setAutoCommit(true);
                finalizaConexao();
            }
        } catch (SQLException ex) {
            Mensagem.ExibirException(ex, "Erro no commit do banco ao Finalizar Transação");
            reverterTransacao();
            return false;
        }
        return true;
    }

    public boolean reverterTransacao() {
        try {
            getCon().rollback();
            getCon().setAutoCommit(true);
        } catch (SQLException ex) {
            Mensagem.ExibirException(ex, "Erro no commit do banco ao Reverter Transação");
            return false;
        }
        return true;
    }

    public int getMaxPK(String tabela, String chave) {
        String sql = "select max(" + chave + ") from " + tabela;
        int max = 0;
        try {
            ResultSet rs = consultar(sql);
            if (rs.next()) {
                max = rs.getInt(1);
            }
        } catch (SQLException sqlex) {
            Mensagem.ExibirException(sqlex);
            setErro("Erro: " + sqlex.toString());
            max = -1;
        }
        return max;
    }

    public String getMensagemErro() {
        return getErro();
    }

    public boolean getEstadoConexao() {
        return (getCon() != null);
    }

    protected static boolean SelecionaConexao(String Driver, String... Params) {
        if (!Driver.equalsIgnoreCase(Conexao.Driver)) {
            return false;
        }
        return true;
    }

    protected static Conexao SelecionaProximo(Conexao Proximo) {
        if (Proximo == null) {
            Mensagem.ExibirException(new Exception("Driver Não Encontrado"),
                    "Driver Não Encontrado");
            return null;
        } else {
            return Proximo;
        }
    }

    /**
     * @return the con
     */
    protected Connection getCon() {
        try {
            if (con == null || con.isClosed()) {
                reconectar();
            }
        } catch (SQLException ex) {
            Mensagem.ExibirException(ex, "Erro no getConnect da Conexao");
        }
        return con;
    }

    public Connection getConnect() {
        return con;
    }

    /**
     * @param con the con to set
     */
    public void setCon(Connection con) {
        this.con = con;
    }

    /**
     * @return the erro
     */
    public String getErro() {
        return erro;
    }

    /**
     * @param erro the erro to set
     */
    public void setErro(String erro) {
        this.erro = erro;
    }

    /**
     * @return the Driver
     */
    public static String getDriver() {
        return Driver;
    }

    /**
     * @param aDriver the Driver to set
     */
    public static void setDriver(String aDriver) {
        Driver = aDriver;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the OnNotManual
     */
    public static boolean isOnNotManual() {
        return OnNotManual;
    }

    /**
     * @param aOnNotManual the OnNotManual to set
     */
    public static void setOnNotManual(boolean aOnNotManual) {
        OnNotManual = aOnNotManual;
    }

    private void finalizaConexao() throws SQLException {
        con.close();
        OpenConection = false;
    }
}
