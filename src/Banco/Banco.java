/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

/**
 *
 * @author Raizen
 */
import Utils.Arquivo;
import Utils.Mensagem;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

public class Banco {

    private static Conexao con = null;
    public static int StatsConexao;
    private static String Driver = "org.postgresql.Driver";
    private static String Host = "jdbc:postgresql://localhost:5432/";
    private static String Banco = "ferro4";
    private static String User = "postgres", Pass = "postgres123";

    private Banco() {

    }

    public static Conexao getCon() {
        conectar();
        return con;
    }

    public static void Desconectar() {
        con = null;
    }

    public static boolean conectar() {
        if (con == null) {
            con = ConexaoDefault.create(Driver);
            return con.conectar(Host, Banco, User, Pass);
        }else{
            con.conectar(Host, Banco, User, Pass);
        }
        return true;
    }

    /**
     * @param nomeBD
     * @param nomeArquivoBanco
     * @param scriptRestauracao "restaurar.bat"
     * @return
     */
    public static int iniciarConexao(String nomeBD, String nomeArquivoBanco, String scriptRestauracao) {
        StatsConexao = PreConexao();
        if (StatsConexao == 0) {
            if (criarBD(nomeBD)) {
                if (criarTabelas(nomeArquivoBanco, nomeBD)) {
                    if (realizaBackupRestauracao(scriptRestauracao)) {
                        Mensagem.ExibirLog("Restore Concluído");
                    } else {
                        Mensagem.ExibirLog("Restore Não Concluído");
                    }
                    StatsConexao = 1;
                } else {
                    Mensagem.ExibirLog("FATAL ERROR: Erro ao criar as tabelas");
                }
            } else {
                Mensagem.ExibirLog(getCon().getMensagemErro());
            }
        }
        return StatsConexao;
    }

    public static int PreConexao(Object... Params) {
        ArrayList<String> Lista = Arquivo.leArquivoDeStringUTF("Config.txt", 6);
        if (Lista != null && !Lista.isEmpty()) {
            return conectar(Lista.get(5), Lista.get(0), Lista.get(1), Lista.get(3),
                    Lista.get(4), Lista.get(2), Params) == true ? 1 : 0;
        }
        return 2;
    }

    public static boolean conectar(String stringConexao, String endereco, String porta,
            String usuarioBanco, String senhaBanco, String banco, Object... Params) {
        if (con == null || 
                (Params != null && Params.length > 0 && Params[0] instanceof Boolean && (Boolean)Params[0] == true)) {
            con = ConexaoDefault.create(Driver);
            Host = stringConexao + "://" + endereco + ":" + porta + "/";
            Banco = banco;
            User = usuarioBanco;
            Pass = senhaBanco;
            return con.conectar(Host, Banco, User, Pass, Params);
        }
        return true;
    }

    public static boolean criarBD(String NomeBD) {
        try {
            conectar();
            Statement statement = getCon().getCon().createStatement();
            statement.execute("CREATE DATABASE " + NomeBD + " WITH OWNER = postgres ENCODING = 'UTF8'  "
                    + "TABLESPACE = pg_default LC_COLLATE = 'Portuguese_Brazil.1252'  "
                    + "LC_CTYPE = 'Portuguese_Brazil.1252'  CONNECTION LIMIT = -1;");
            statement.close();
        } catch (SQLException e) {
            Mensagem.ExibirException(e);
            return false;
        }
        return true;
    }
    
    public static boolean executarSql(String Sql){
        try {
            Statement statement = getCon().getCon().createStatement();
            statement.execute(Sql);
            statement.close();
        } catch (SQLException e) {
            Mensagem.ExibirException(e);
             return false;
        }
        return true;
    }

    public static boolean criarTabelas(String Arquivo) {
        try {
            Statement statement = getCon().getCon().createStatement();
            RandomAccessFile arq = new RandomAccessFile(Arquivo, "r");
            while (arq.getFilePointer() < arq.length()) {
                statement.addBatch(arq.readLine());
            }
            statement.executeBatch();
            statement.close();
        } catch (IOException | SQLException e) {
            Mensagem.ExibirException(e, "Erro ao criar tabela: ");
            return false;
        }
        return true;
    }

    public static boolean realizaBackupRestauracao(String arqlote, TextArea ta) {
        Runtime r = Runtime.getRuntime();
        try {
            Process p = r.exec("bkp\\" + arqlote);
            if (p != null) {
                InputStreamReader str = new InputStreamReader(p.getErrorStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null) {
                    System.out.println(linha);
                    final String lin = linha;
                    Platform.runLater(()
                            -> {
                        ta.appendText(lin + "\n");
                    });
                }
                ta.appendText("Completo....." + "\n");
            }
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Realizar Backup");
            return false;
        }
        return true;
    }

    public static boolean realizaBackupRestauracao(String arqlote) {
        Runtime r = Runtime.getRuntime();
        try {
            Process p = r.exec("bkp\\" + arqlote);
            if (p != null) {
                InputStreamReader str = new InputStreamReader(p.getErrorStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null) {
                    System.out.println(linha);
                }
                System.out.println("Completo....." + "\n");
            }
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Realizar Restauração");
            return false;
        }
        return true;
    }

    public static boolean criarTabelas(String script, String BD) {
        try {
            //Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost/" + BD;
            Connection con = DriverManager.getConnection(url, "postgres", "postgres123");

            Statement statement = con.createStatement();
            RandomAccessFile arq = new RandomAccessFile(script, "r");
            while (arq.getFilePointer() < arq.length()) {
                statement.addBatch(arq.readLine());
            }
            statement.executeBatch();

            statement.close();
            con.close();
        } catch (IOException | SQLException e) {
            Mensagem.ExibirException(e, "Erro ao criar tabela: ");
            return false;
        }
        return true;
    }

    public static boolean criarBancoD(String BD) {
        try {
            //Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost/";
            Connection con = DriverManager.getConnection(url, "postgres", "postgres123");

            Statement statement = con.createStatement();
            statement.execute("CREATE DATABASE " + BD + " WITH OWNER = postgres ENCODING = 'UTF8'  "
                    + "TABLESPACE = pg_default LC_COLLATE = 'Portuguese_Brazil.1252'  "
                    + "LC_CTYPE = 'Portuguese_Brazil.1252'  CONNECTION LIMIT = -1;");
            statement.close();
            con.close();
        } catch (SQLException e) {
            Mensagem.ExibirException(e);
            return false;
        }
        return true;
    }

    /**
     * @return the Driver
     */
    public static String getDriver() {
        return Driver;
    }

    /**
     * @param aDriver the Driver to set
     */
    public static void setDriver(String aDriver) {
        Driver = aDriver;
    }

    /**
     * @return the Host
     */
    public static String getHost() {
        return Host;
    }

    /**
     * @param aHost the Host to set
     */
    public static void setHost(String aHost) {
        Host = aHost;
    }

    /**
     * @return the Banco
     */
    public static String getBanco() {
        return Banco;
    }

    /**
     * @param aBanco the Banco to set
     */
    public static void setBanco(String aBanco) {
        Banco = aBanco;
    }

    /**
     * @return the User
     */
    public static String getUser() {
        return User;
    }

    /**
     * @param aUser the User to set
     */
    public static void setUser(String aUser) {
        User = aUser;
    }

    /**
     * @return the Pass
     */
    public static String getPass() {
        return Pass;
    }

    /**
     * @param aPass the Pass to set
     */
    public static void setPass(String aPass) {
        Pass = aPass;
    }

}
