/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import java.io.InputStream;
import java.util.Scanner;

/**
 *
 * @author Raizen
 */
public abstract class GenerateSQL {

    public boolean Generate(String Nome) {
        return Banco.executarSql(Ler(Substitui("/" + this.getClass().getPackage().getName() + "/" + Nome)));
    }
    
    public boolean Drop_Table(){
        return Banco.executarSql(Ler(Substitui("/" + this.getClass().getPackage().getName() + "/" + "Sql_Drop")));
    }

    protected String Substitui(String Nome) {
        String aux = "";
        for (int i = 0; i < Nome.length(); i++) {
            if (Nome.charAt(i) == '.') {
                aux += "/";
            } else {
                aux += Nome.charAt(i);
            }
        }
        return aux;
    }

    protected String Ler(String Arquivo) {
        String string = "";
        InputStream IS = getClass().getResourceAsStream(Arquivo);
        Scanner input = new Scanner(IS);
        while (input.hasNextLine()) {
            string += input.nextLine() + "\n";
        }
        return string;
    }
}
