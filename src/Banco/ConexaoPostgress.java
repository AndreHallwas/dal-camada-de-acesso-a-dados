/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Raizen
 */
public class ConexaoPostgress extends Conexao implements Factory{

    public static Conexao create(String Driver, String... Params) {
        ConexaoPostgress.setDriver("org.postgresql.Driver");
        boolean Resultado = SelecionaConexao(Driver, Params);
        if (Resultado == true) {
            return (Conexao) new ConexaoPostgress(Driver, Params);
        } else {
            /**
             * Se Houver proximo retorna ele, se não houver retorna null
             * Classe.create();
             */
            return SelecionaProximo(null);
        }
    }

    private ConexaoPostgress(String Driver, String... Params) {
        super(Params);
    }

    @Override
    protected void ConexaoEspecifica(String local, String banco, String usuario, String senha,
            String Driver, Object... Params) throws ClassNotFoundException, SQLException {
        /*Class.forName(Driver);*/
        String url = local + banco; //"jdbc:postgresql://localhost/"+banco;
        setCon(DriverManager.getConnection(url, usuario, senha));
    }
}
