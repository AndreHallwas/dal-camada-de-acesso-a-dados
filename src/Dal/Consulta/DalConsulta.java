package Dal.Consulta;

import Banco.Banco;
import Dal.DAL;
import Utils.Imagem;
import Dal.Indice.IndiceDal;
import Utils.Mensagem;
import Utils.Reflect;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DalConsulta extends DAL {

    private Object Objeto;
    private static final ArrayList<DalConsultaEspecifica> ConsultaEspecifica = new ArrayList();

    protected String getCodigo(String Campo, String Filtro, boolean op) {
        String sql = "";
        if (!Filtro.isEmpty()) {
            try {
                int aux = Integer.parseInt(0 + Filtro);
                if (op) {
                    sql = sql + " or " + Campo + " = '" + aux + "'";
                } else {
                    sql = sql + " " + Campo + " = '" + aux + "'";
                }
                return sql;
            } catch (NumberFormatException ex) {
                Mensagem.ExibirException(ex, "Erro no Nivel Cod");
            }
        }
        return "";
    }

    public static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (Character.isDigit(str.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    protected Object getParameterType(ResultSet rs, int j) {
        try {
            switch (Indices.get(j).getTipo()) {
                case "int":
                    return rs.getInt((String) Indices.get(j).getIndice());
                case "str":
                    return rs.getString((String) Indices.get(j).getIndice());
                case "date":
                    return rs.getDate((String) Indices.get(j).getIndice());/*LocalDate*/
                case "double":
                    return rs.getDouble((String) Indices.get(j).getIndice());
                case "img":
                    return Imagem.ByteArrayToBufferedImage(rs.getBytes((String) Indices.get(j).getIndice()));
                default:
                    return rs.getObject((String) Indices.get(j).getIndice());
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
        }
        return null;
    }

    /**
     * @param Filtro
     * @param Params not null is a advanced find, [0] is the index of the
     * advanced find, Param[0] is a code of Specific find. 0 to defined 1 for
     * fields;
     * @return
     */
    public ArrayList<Object> get(String Filtro, String... Params) {
        String sql = "";
        ArrayList<Object> Entidades = new ArrayList();
        try {
            sql = "select * from " + Tabela + " ";
            if (!Filtro.isEmpty()) {
                if (Params == null || Params.length < 1) {
                    sql += DefaultSql(Filtro);
                } else {
                    if (Params[0].equals("1")) {
                        sql += DefaultSqlFromFields(Filtro, Params);
                    } else {
                        sql += AdvancedSql(Filtro, Params);
                    }
                }
            }
            Entidades = Consulta(sql);
        } catch (Exception ex) {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return Entidades;
    }

    protected String DefaultSqlFromFields(String Filtro, String... Params) {
        String sql = "";
        First = false;
        if (Indices.size() > 0 && Params != null && Params.length > 0) {
            ArrayList<IndiceDal> Aux = new ArrayList();
            String Param;
            for (int i = 1; i < Params.length; i++) {
                Param = Params[i];
                int Pos = 0;
                while (Pos < Indices.size() && !Indices.get(Pos).getCampo().equals(Param)) {
                    Pos++;
                }
                if (Pos < Indices.size()) {
                    Aux.add(Indices.get(Pos));
                }
            }
            sql += "where ";
            for (IndiceDal Indice : Aux) {
                sql = getSqlFromIndex(Indice, Filtro, sql);
            }
        }
        return sql;
    }

    protected String AdvancedSql(String Filtro, String... Params) {
        String sql = "";
        if (!ConsultaEspecifica.isEmpty()) {
            DalConsultaEspecifica aux;
            int i = 0;
            while (i < ConsultaEspecifica.size() && i <= Integer.parseInt(Params[0])) {
                i++;
            }
            aux = (i < ConsultaEspecifica.size()) ? ConsultaEspecifica.get(i) : null;
            sql += aux == null ? " " : aux.AdvancedSql(Params);
        }
        return sql;
    }

    protected boolean First;

    protected String DefaultSql(String Filtro) {
        String sql = "";
        First = false;
        if (Indices.size() > 0) {
            sql += "where ";
            for (IndiceDal Indice : Indices) {
                sql = getSqlFromIndex(Indice, Filtro, sql);
            }
        }
        return sql;
    }

    protected String getSqlFromIndex(IndiceDal indice, String Filtro, String sql) {
        if (indice.getChave() != '5') {
            if ("int".equals(indice.getTipo()) || "double".equals(indice.getTipo())) {
                sql += getCodigo(indice.getIndice(), Filtro, First);
                First = !sql.isEmpty();
            } else if (First) {
                if ("date".equals(indice.getTipo())) {
                    if (Filtro.length() > 8) {
                        sql += " or " + indice.getIndice() + " = '" + Filtro + "'";
                    }
                } else if ("img".equals(indice.getTipo())) {
                    sql += " or " + indice.getIndice() + " = '" + Filtro + "'";
                } else if ("boolean".equals(indice.getTipo())) {
                    sql += getBoolean(indice, Filtro);
                } else {
                    sql += " or upper(" + indice.getIndice() + ") like upper('" + Filtro + "%') ";
                }
            } else if (!First) {
                if ("date".equals(indice.getTipo())) {
                    if (Filtro.length() > 8) {
                        sql += " " + indice.getIndice() + " = '" + Filtro + "'";
                        First = true;
                    }
                } else if ("img".equals(indice.getTipo())) {
                    sql += " " + indice.getIndice() + " = '" + Filtro + "'";
                    First = true;
                } else if ("boolean".equals(indice.getTipo())) {
                    sql += getBoolean(indice, Filtro);
                    First = true;
                } else {
                    sql += " upper(" + indice.getIndice() + ") like upper('" + Filtro + "%') ";
                    First = true;
                }
            }
        }
        return sql;
    }

    protected String getBoolean(IndiceDal indice, String Filtro) {
        String sql = "";
        if (Filtro instanceof String) {
            if (isNumeric(Filtro)) {
                try {
                    int aux = Integer.parseInt(Filtro);
                    if (aux == 0) {
                        sql += " or " + indice.getIndice() + " = '" + "0" + "'";
                    } else {
                        sql += " or " + indice.getIndice() + " = '" + "1" + "'";
                    }
                } catch (Exception ex) {
                    Mensagem.ExibirException(ex, "Erro de boolean na Consulta");
                }
            } else if (Filtro.equalsIgnoreCase("true")) {
                sql += " or " + indice.getIndice() + " = '" + "1" + "'";
            } else {
                sql += " or " + indice.getIndice() + " = '" + "0" + "'";
            }
        }
        return sql;
    }

    protected ArrayList<Object> Consulta(String sql) {
        Object ce = null;
        ArrayList<Object> Entidades = new ArrayList();
        pstmt = Banco.getCon().geraStatement(sql);
        ResultSet rs = Banco.getCon().consultar(pstmt);
        try {
            while (rs.next()) {
                ce = Reflect.getObject(getObjeto().getClass().getName(), getObjeto().getClass(), " ", 1, "");
                for (int i = 0; i < Indices.size(); i++) {
                    Reflect.getObject("", ce, "set" + Indices.get(i).getCampo(), 0, getParameterType(rs, i));
                }
                Entidades.add(ce);
            }
            setMsg(true, "Consulta");
        } catch (SQLException ex) {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return Entidades;
    }

    /**
     * @return the Objeto
     */
    public Object getObjeto() {
        return Objeto;
    }

    /**
     * @param Objeto the Objeto to set
     */
    public void setObjeto(Object Objeto) {
        this.Objeto = Objeto;
    }

}
