package Dal.Transacao.Flyweight;

import Banco.Banco;
import Dal.DAL;

public class FlyRemover {

    public static boolean Transacao(String Tabela, DAL dal, boolean flag, String... Param) {
        int i = 0;
        while (i < dal.getIndices().size() && !(dal.getIndices().get(i).getChave() == '1')) {
            i++;
        }
        if (i < dal.getIndices().size()) {
            dal.setPstmt(Banco.getCon().geraStatement("delete from " + Tabela + " where " + dal.getIndices().get(i).getIndice()
                    + " = '" + dal.getIndices().get(i).getAtributoDal() + "'"));
            dal.setMsg(flag = Banco.getCon().manipular(dal.getPstmt()), "Exclusão");
        }
        return flag;
    }

}
