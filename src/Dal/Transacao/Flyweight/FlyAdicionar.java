package Dal.Transacao.Flyweight;

import Banco.Banco;
import Dal.Transacao.Transacao;

public class FlyAdicionar {

    public static boolean Transacao(String Tabela, Transacao dal, boolean flag, String... Param) {
        int Pos = 0;
        /////dal.setIndices(Indices, objeto);
        if (dal.getIndices().size() > 0 && dal.getControl() == 1) {
            String sqlCampos = "", sqlParametros = "";

            while (Pos < dal.getIndices().size() && (dal.getIndices().get(Pos).getAtributoDal() == null
                    || dal.getIndices().get(Pos).getAtributoDal().toString().isEmpty() 
                    || dal.getIndices().get(Pos).getChave() == '5')) {
                Pos++;
            }
            if (Pos < dal.getIndices().size()) {
                sqlCampos += "(" + dal.getIndices().get(Pos).getIndice();
                sqlParametros += "(?";
                for (int i = Pos + 1; i < dal.getIndices().size(); i++) {
                    if (dal.getIndices().get(i).getAtributoDal() != null && dal.getIndices().get(i).getChave() != '5') {
                        sqlCampos += "," + dal.getIndices().get(i).getIndice();
                        sqlParametros += ",?";
                    }
                }
                sqlCampos += ")";
                sqlParametros += ")";
                dal.setPstmt(Banco.getCon().geraStatement("insert into " + Tabela + " " + sqlCampos + " values" + sqlParametros));
                dal.setParametros();
                dal.setMsg(flag = Banco.getCon().manipular(dal.getPstmt()), "Cadastro");
            }
        }
        return flag;
    }

}
