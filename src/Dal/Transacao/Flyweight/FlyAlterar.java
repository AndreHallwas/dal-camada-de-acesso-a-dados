package Dal.Transacao.Flyweight;

import Banco.Banco;
import Dal.Transacao.Transacao;

public class FlyAlterar {

    public static boolean Transacao(String Tabela, Transacao dal, boolean flag, String... Param) {
        int Pos = 0;
        if (dal.getIndices().size() > 0 && dal.getControl() == 1) {
            while (Pos < dal.getIndices().size() &&  (dal.getIndices().get(Pos).getAtributoDal() == null
                    || dal.getIndices().get(Pos).getAtributoDal().toString().isEmpty() 
                    || dal.getIndices().get(Pos).getChave() == '5')) {
                Pos++;
            }
            if (Pos < dal.getIndices().size()) {
                String sqlCampos = "", sqlParam = "";
                sqlCampos = dal.getIndices().get(Pos).getIndice() + " = ?";
                if (dal.getIndices().get(Pos).getChave() == '1') {
                    sqlParam = dal.getIndices().get(Pos).getIndice() + " = '" + dal.getIndices().get(Pos).getAtributoDal() + "'";
                }
                for (int i = Pos + 1; i < dal.getIndices().size(); i++) {
                    if (dal.getIndices().get(i).getAtributoDal() != null && dal.getIndices().get(i).getChave() != '5') {
                        sqlCampos += ", " + dal.getIndices().get(i).getIndice() + " = ?";
                        if (dal.getIndices().get(i).getChave() == '1') {
                            sqlParam = dal.getIndices().get(i).getIndice() + " = '" + dal.getIndices().get(i).getAtributoDal() + "'";
                        }
                    }
                }
                sqlCampos += " where " + sqlParam;
                dal.setPstmt(Banco.getCon().geraStatement("update " + Tabela + " set " + sqlCampos));
                dal.setParametros();
                dal.setMsg(flag = Banco.getCon().manipular(dal.getPstmt()), "Alteração");
            }
        }
        return flag;
    }

}
