package Dal.Transacao;

import Dal.Transacao.Flyweight.FlyRemover;

public class DalRemover extends Transacao {

    @Override
    public boolean Transacao() {
        boolean Resultado = super.Transacao() && (flag = FlyRemover.Transacao(Tabela, this, flag));
        return verificaFinalizacao() && Resultado;
    }

}
