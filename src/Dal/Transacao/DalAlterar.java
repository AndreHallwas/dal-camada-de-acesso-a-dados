package Dal.Transacao;

import Dal.Transacao.Flyweight.FlyAlterar;

public class DalAlterar extends Transacao {

    @Override
    public boolean Transacao() {
        boolean Resultado = super.Transacao() && (flag = FlyAlterar.Transacao(Tabela, this, flag));
        return verificaFinalizacao() && Resultado;
    }

}
