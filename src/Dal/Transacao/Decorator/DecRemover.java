package Dal.Transacao.Decorator;

import Dal.Transacao.DalTransacao;
import Dal.Transacao.Flyweight.FlyRemover;

public class DecRemover extends DalTransacaoDecorator {

    public DecRemover(DalTransacao transacao) {
        super(transacao);
    }

    @Override
    public boolean Transacao() {
        boolean Resultado = super.Transacao() && super.getDalTransacao().Transacao() && (flag = FlyRemover.Transacao(Tabela, this, flag));
        return super.verificaFinalizacao() && Resultado;
    }

}
