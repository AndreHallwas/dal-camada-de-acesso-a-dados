package Dal.Transacao.Decorator;

import Dal.Transacao.DalTransacao;
import Dal.Transacao.Flyweight.FlyAdicionar;

public class DecAdicionar extends DalTransacaoDecorator {

    public DecAdicionar(DalTransacao transacao) {
        super(transacao);
    }

    @Override
    public boolean Transacao() {
        boolean Resultado = super.Transacao() && super.getDalTransacao().Transacao() && (flag = FlyAdicionar.Transacao(Tabela, this, flag));
        return super.verificaFinalizacao() && Resultado;
        
    }

}
