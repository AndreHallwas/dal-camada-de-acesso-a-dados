package Dal.Transacao.Decorator;

import Dal.Transacao.DalTransacao;
import Dal.Transacao.Flyweight.FlyAlterar;

public class DecAlterar extends DalTransacaoDecorator {

    public DecAlterar(DalTransacao transacao) {
        super(transacao);
    }

    @Override
    public boolean Transacao() {
        boolean Resultado = super.Transacao() && super.getDalTransacao().Transacao() && (flag = FlyAlterar.Transacao(Tabela, this, flag));
        return super.verificaFinalizacao() && Resultado;
    }

}
