package Dal.Transacao.Decorator;

import Dal.Transacao.DalTransacao;
import Dal.Transacao.Transacao;
import Dal.Utils.DalMaxPKTableKey;

public abstract class DalTransacaoDecorator extends Transacao implements DalTransacao {

    protected DalTransacao dalTransacao;

    @Override
    public boolean Transacao() {
        return OptionalUtils();
    }

    public DalTransacaoDecorator(DalTransacao transacao) {
        transacao.setFinal(0);
        dalTransacao = transacao;
    }

    public DalTransacao getDalTransacao() {
        return dalTransacao;
    }

    public void setDalTransacao(DalTransacao dalTransacao) {
        this.dalTransacao = dalTransacao;
    }

}
