/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dal.Transacao;

import Banco.Banco;
import Dal.DAL;
import Dal.Utils.DalMaxPKTableKey;
import Dal.Utils.DalUtils;
import Utils.Imagem;
import Utils.Mensagem;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.sql.Date;
import java.time.LocalDate;

/**
 *
 * @author Aluno
 */
public abstract class Transacao extends DAL implements DalTransacao {

    protected int Control = 1;
    protected int Final = 1;
    protected DalUtils MaxPKTableKey;
    
    public Transacao() {
    }

    @Override
    public boolean Transacao() {
        return iniciarTransacao() && OptionalUtils();
    }

    protected boolean iniciarTransacao() {
        return Banco.getCon().iniciarTransacao();
    }

    protected boolean finalizarTransacao() {
        return Banco.getCon().finalizarTransacao();
    }

    protected boolean reverterTransacao() {
        return Banco.getCon().reverterTransacao();
    }

    protected boolean verificaFinalizacao() {
        return (flag == true ? true : reverterTransacao()) && (Final == 1 ? finalizarTransacao() : true);
    }
    
    public void OnMaxPKTableKeyNecessary(String Campo, Object Objeto){
        MaxPKTableKey = DalMaxPKTableKey.create(this, Campo, Objeto);
        MaxPKTableKey.setEnable(true);
    }

    @Override
    public boolean setParametros() {
        flag = true;
        try {
            for (int i = 1, j = 0; j < Indices.size(); i++, j++) {
                if (!(Indices.get(j).getAtributoDal() == null
                        || Indices.get(j).getAtributoDal().toString().isEmpty() || Indices.get(j).getChave() == '5')) {
                    switch (Indices.get(j).getTipo()) {
                        case "int":
                            pstmt.setInt(i, Integer.parseInt(Indices.get(j).getAtributoDal() + ""));
                            break;
                        case "str":
                            pstmt.setString(i, (String) Indices.get(j).getAtributoDal());
                            break;
                        case "date":
                            pstmt.setDate(i, (Date.valueOf((LocalDate) Indices.get(j).getAtributoDal())));/*LocalDate*/
                            break;
                        case "double":
                            pstmt.setDouble(i, Double.parseDouble(Indices.get(j).getAtributoDal() + ""));
                            break;
                        case "img":
                            byte[] a = {1};
                            pstmt.setBinaryStream(i, (Indices.get(j).getAtributoDal() != null)
                                    ? Imagem.BufferedImageToInputStream((BufferedImage) Indices.get(j).getAtributoDal())
                                    : new ByteArrayInputStream((a)));
                            break;
                        default:
                            pstmt.setObject(i, Indices.get(j).getAtributoDal());
                            break;
                    }
                } else {
                    i--;
                }
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
            flag = false;
        }
        return flag;
    }

    protected boolean OptionalUtils() {
        return (MaxPKTableKey != null && MaxPKTableKey.isEnable()) ? MaxPKTableKey.work() : true;
    }

    public int getFinal() {
        return Final;
    }

    @Override
    public void setFinal(int Final) {
        this.Final = Final;
    }

    public int getControl() {
        return Control;
    }

    public void setControl(int Control) {
        this.Control = Control;
    }

}
