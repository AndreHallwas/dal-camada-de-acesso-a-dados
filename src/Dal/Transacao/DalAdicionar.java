package Dal.Transacao;

import Dal.Transacao.Flyweight.FlyAdicionar;

public class DalAdicionar extends Transacao {

    @Override
    public boolean Transacao() {
        boolean Resultado = super.Transacao() && (flag = FlyAdicionar.Transacao(Tabela, this, flag));
        return  verificaFinalizacao() && Resultado;
    }

}
