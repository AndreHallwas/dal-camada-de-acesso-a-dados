/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dal.Utils;

import Dal.DAL;
import Dal.Indice.IndiceDal;
import Dal.Transacao.Transacao;
import Utils.Reflect;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public final class DalMaxPKTableKey implements DalUtils {
    /**
     * @param Params [0] Transacao, [1] Campo, [2] Objeto;
     * @return 
     */
    public static DalMaxPKTableKey create(Object... Params) {
        if (Params != null && Params.length > 0) {
            return new DalMaxPKTableKey((Transacao) Params[0], (String) Params[1], Params[2]);
        }
        return new DalMaxPKTableKey();
    }

    private String Campo;
    private Object objeto;
    private DAL Transacao;
    private boolean Enable = false;

    private DalMaxPKTableKey() {
    }

    private DalMaxPKTableKey(Transacao Transacao, String Campo, Object Objeto) {
        setTransacao(Transacao);
        setCampo(Campo);
        setObjeto(Objeto);
    }

    @Override
    public final boolean work(String... Params) {
        setEnable(false);
        if (Campo != null && !Campo.isEmpty() && objeto != null && Transacao != null) {
            Object Atributo = Banco.Banco.getCon().getMaxPK(Transacao.getTabela(), Campo);
            return FindandReplace(Atributo) && ReflectInObject(Atributo);
        }
        return false;
    }

    protected boolean FindandReplace(Object Atributo) {
        ArrayList<IndiceDal> Indices = Transacao.getIndices();
        int i = 0;
        while (i < Indices.size() && !Indices.get(i).getCampo().equalsIgnoreCase(Campo)) {
            i++;
        }
        if (i < Indices.size()) {
            Indices.get(i).setAtributo(Atributo);
            return true;
        }
        return false;
    }

    protected boolean ReflectInObject(Object Atributo) {
        return (boolean) Reflect.getObject("", objeto, "set" + Campo, 0, Atributo);
    }

    /**
     * @param aCampo the Campo to set
     */
    public void setCampo(String aCampo) {
        Campo = aCampo;
    }

    /**
     * @param aObjeto the objeto to set
     */
    public void setObjeto(Object aObjeto) {
        objeto = aObjeto;
    }

    /**
     * @param aTransacao the Transacao to set
     */
    public void setTransacao(DAL aTransacao) {
        Transacao = aTransacao;
    }

    /**
     * @param aEnable the Enable to set
     */
    @Override
    public void setEnable(boolean aEnable) {
        Enable = aEnable;
    }

    /**
     * @return the Enable
     */
    @Override
    public boolean isEnable() {
        return Enable;
    }
}
