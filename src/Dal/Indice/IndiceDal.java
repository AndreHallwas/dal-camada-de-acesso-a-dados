/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dal.Indice;

/**
 *
 * @author Raizen
 */
public class IndiceDal {
    private Object Atributo;
    private String Indice;
    private String Tipo;
    private String Nome;
    private String Campo;
    private char Chave;

    public IndiceDal(Object Atributo, String Tipo, String Nome) {
        this.Atributo = Atributo;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Campo = Nome;
        this.Indice = Nome;
    }

    public IndiceDal(Object Atributo, String Indice, String Tipo, String Nome) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Campo = Nome;
        /////Chave = '0';
    }
    
    public IndiceDal(Object Atributo, String Indice, String Tipo, String Nome, Object Campo) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Campo = Campo.toString();
        /////Chave = '0';
    }

    public IndiceDal(Object Atributo, String Indice, String Tipo, char Chave, String Nome) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Campo = Nome;
        this.Chave = Chave;
    }
    
    public IndiceDal(Object Atributo, String Indice, String Tipo, char Chave, String Nome, Object Campo) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Chave = Chave;
        this.Campo = Campo.toString();
    }

    /**
     * @return the Atributo
     */
    public Object getAtributo() {
        return Atributo;
    }
    public Object getAtributoDal(){
        if(Atributo != null && Atributo instanceof String && Atributo.toString().isEmpty()){
            return null;
        }
        return Atributo;
    }

    /**
     * @param Atributo the Atributo to set
     */
    public void setAtributo(Object Atributo) {
        this.Atributo = Atributo;
    }

    /**
     * @return the Indice
     */
    public String getIndice() {
        return Indice;
    }

    /**
     * @param Indice the Indice to set
     */
    public void setIndice(String Indice) {
        this.Indice = Indice;
    }

    /**
     * @return the Tipo
     */
    public String getTipo() {
        return Tipo;
    }

    /**
     * @param Tipo the Tipo to set
     */
    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the Campo
     */
    public String getCampo() {
        return Campo;
    }

    /**
     * @param Campo the Campo to set
     */
    public void setCampo(String Campo) {
        this.Campo = Campo;
    }

    /**
     * @return the Chave
     */
    public char getChave() {
        return Chave;
    }

    /**
     * @param Chave the Chave to set
     */
    public void setChave(char Chave) {
        this.Chave = Chave;
    }
    
    
}
