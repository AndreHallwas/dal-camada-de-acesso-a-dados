package Dal.Indice;

import java.util.ArrayList;

public interface IndicesDal {
    public abstract ArrayList<IndiceDal> getIndices();
}
