/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dal.DDL;

import Dal.Entidade.ControledeEntidade;
import Dal.Indice.IndiceDal;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class DDL {

    private ArrayList<IndiceDal> Indices;
    private String Tabela;
    private String Sql = "";
    
    
    public static String create(ControledeEntidade ce){
        DDL ddl = new DDL();
        ddl.setIndices(ce.getIndices());
        ddl.setTabela(ce.getTabela());
        ddl.Main();
        return ddl.getSql();
    }
    
    protected void Main() {
        setSql(getSql() + Create_Table(getTabela()));
        String SqlConstraint = "";
        String SqlConstraintFinalize = "";
        boolean First = true;
        for (IndiceDal Indice : getIndices()) {
            setSql(getSql() + Create_Collumn(Indice.getIndice(), Indice.getTipo(), Indice.getChave()));
            if (Indice.getChave() == '1') {
                if(First){
                    SqlConstraint = "  Constraint primary key ( " + Indice.getIndice() + " ";
                    SqlConstraintFinalize = " ) ";
                    First = false;
                }else{
                    SqlConstraint += ", " + Indice.getIndice();
                }
            }
        }
        SqlConstraint += SqlConstraintFinalize;
        setSql(getSql() + SqlConstraint);
        setSql(getSql() + " ); ");
    }

    protected String Create_Table(String Tabela) {
        return "Create Table " + Tabela + "(";
    }

    protected String Create_Collumn(String Nome, String Tipo, char Chave) {
        String aux = "    " + Nome + " ";
        if (Chave == '1' && Tipo.equals("int")) {
            aux += "Serial not null ,";
        } else if (Chave == '1' && Tipo.equals("str")) {
            aux += "Serial not null ,";
        } else if (Tipo.equals("int")) {
            aux += "Integer null ,";
        } else if (Tipo.equals("date")) {
            aux += "Date null ,";
        } else if (Tipo.equals("double")) {
            aux += "Decimal(10,2) null ,";
        } else if (Tipo.equals("img")) {
            aux += "oid null ,";
        } else if (Tipo.equals("str")) {
            aux += "varchar(200) null ,";
        }
        return aux;
    }

    /**
     * @return the Indices
     */
    public ArrayList<IndiceDal> getIndices() {
        return Indices;
    }

    /**
     * @param Indices the Indices to set
     */
    public void setIndices(ArrayList<IndiceDal> Indices) {
        this.Indices = Indices;
    }

    /**
     * @return the Tabela
     */
    public String getTabela() {
        return Tabela;
    }

    /**
     * @param Tabela the Tabela to set
     */
    public void setTabela(String Tabela) {
        this.Tabela = Tabela;
    }

    /**
     * @return the Sql
     */
    public String getSql() {
        return Sql;
    }

    /**
     * @param Sql the Sql to set
     */
    public void setSql(String Sql) {
        this.Sql = Sql;
    }

}
