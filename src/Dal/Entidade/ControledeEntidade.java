/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dal.Entidade;

import Dal.Builder.DalBuilder;
import Dal.Consulta.DalConsulta;
import Dal.Indice.IndiceDal;
import Dal.Indice.IndicesDal;
import Utils.Mensagem;
import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class ControledeEntidade implements Entidade, IndicesDal {

    protected ArrayList<IndiceDal> Indices;
    protected boolean Final = true;
    protected String Tabela;

    public abstract void setTabela();

    public ControledeEntidade() {
        setTabela();
        setctrEntidade();
    }

    @Override
    public boolean add() {
        setctrEntidade();
        DalBuilder.add();
        return DalBuilder.Transacao(Final);
    }

    @Override
    public boolean altera() {
        setctrEntidade();
        DalBuilder.alterar();
        return DalBuilder.Transacao(Final);
    }

    @Override
    public boolean remove() {
        setctrEntidade();
        DalBuilder.remover();
        return DalBuilder.Transacao(Final);
    }

    @Override
    public ArrayList<Object> get(String filtro, String... Params) {
        setctrEntidade();
        DalConsulta tr = DalBuilder.get();
        return tr.get(filtro, Params);
    }

    public boolean isFinal() {
        return Final;
    }

    public void setFinal(boolean Final) {
        this.Final = Final;
    }

    public void Inicializa(Object instancia, String categoria) {
        DalBuilder.Inicializa(instancia, categoria);
    }

    protected void setctrEntidade() {
        Inicializa(this, getTabela());
    }

    protected void getCampos(Class classe, int... Params) {
        try {
            Field Campo;
            this.getClass();
            if (Params == null) {
                for (int i = 0; i < classe.getDeclaredFields().length; i++) {
                    Campo = classe.getDeclaredFields()[i];
                    Indices.set(i, (IndiceDal) Campo.get(this));
                }
            } else {
                if (Params[0] == 1) {
                    for (Field declaredField : classe.getDeclaredFields()) {
                        Campo = declaredField;
                        Indices.add(new IndiceDal(Campo.get(this), Campo.getType().getName(), Campo.getName()));
                    }
                }
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro no getCampos");
        }
    }

    @Override
    public final ArrayList<IndiceDal> getIndices() {
        if (Indices == null) {
            Indices = new ArrayList();
            getIndicesEmpty();
        } else {
            getIndicesNotEmpty();
        }

        return Indices;
    }

    protected void getIndicesEmpty() {
        getCampos(this.getClass());
    }

    protected void getIndicesNotEmpty() {
        getCampos(this.getClass(), 1);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        Class<?> thisClass = null;
        try {
            thisClass = Class.forName(this.getClass().getName());

            Field[] aClassFields = thisClass.getDeclaredFields();
            sb.append(this.getClass().getSimpleName()).append(" [ ");
            for (Field f : aClassFields) {
                String fName = f.getName();
                sb.append("(").append(f.getType()).append(") ").append(fName).append(" = ").append(f.get(this)).append(", ");
            }
            sb.append("]");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    protected Object verificaNull(Object Objeto) {
        return Objeto == null ? null : Objeto;
    }

    /**
     * @return the Tabela
     */
    public String getTabela() {
        return Tabela;
    }

}
