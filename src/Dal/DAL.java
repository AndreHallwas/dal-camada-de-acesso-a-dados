package Dal;

import Dal.Memento.Estado;
import Dal.Indice.IndiceDal;
import Banco.Banco;
import Dal.Indice.IndicesDal;
import Dal.Memento.Atributos;
import Utils.Mensagem;
import java.sql.PreparedStatement;
import java.util.ArrayList;

public abstract class DAL {

    protected boolean flag = true;
    protected PreparedStatement pstmt;
    protected String Msg;
    protected String Tabela;
    protected ArrayList<IndiceDal> Indices;
    protected static ArrayList<Estado> Estados = new ArrayList();
    protected Atributos atributos;

    public void setMsg(boolean Condition, String Op) {
        if (Condition) {
            Msg = Op + " Efetuado";
            flag = true;
        } else {
            Msg = Op + " Não Efetuado Erro: " + Banco.getCon().getMensagemErro();
        }
        Mensagem.ExibirLog(Msg);
    }
    
    protected void salvarEstado() {
        Estado estado = new Estado();
        estado.setEstado(Indices, Tabela);
        Estados.add(estado);
    }

    protected boolean restaurarEstado() {
        int i = 0;
        while (i < Estados.size() && !Estados.get(i).isEntidade(Tabela)) {
            i++;
        }
        Indices = i < Estados.size() ? Estados.get(i).getEstado() : null;
        return Indices != null;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public PreparedStatement getPstmt() {
        return pstmt;
    }

    public void setPstmt(PreparedStatement pstmt) {
        this.pstmt = pstmt;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String Msg) {
        this.Msg = Msg;
    }

    public String getTabela() {
        return Tabela;
    }

    public void setTabela(String Tabela) {
        this.Tabela = Tabela;
    }

    public ArrayList<IndiceDal> getIndices() {
        if (Indices == null) {
            restaurarEstado();
        }
        getAtributos();
        return Indices;
    }

    public boolean setIndices(ArrayList<IndiceDal> Indices, IndicesDal objeto) {
        int i = 0;
        while (i < Estados.size() && !Estados.get(i).isEntidade(Tabela)) {
            i++;
        }
        if (!(i < Estados.size())) {
            this.Indices = objeto.getIndices();
            salvarEstado();
            return false;
        }else{
           this.Indices = Indices;/////pode ser que seja nulo, possivel problema
        }
        if(atributos == null){
            this.Indices = objeto.getIndices();
            setAtributos();
        }
        return true;
    }
    
    protected void getAtributos(){
        if(atributos != null){
            for (int i = 0; i < Indices.size(); i++) {
                Indices.get(i).setAtributo(atributos.getEstado(i));
            }
        }
    }
    
    protected void setAtributos(){
        atributos = new Atributos();
        ArrayList<Object> objetos = new ArrayList();
        for (int i = 0; i < Indices.size(); i++) {
            objetos.add(Indices.get(i).getAtributo());
        }
        atributos.setEstado(objetos);
    }

    public ArrayList<Estado> getEstados() {
        return Estados;
    }

    public void setEstados(ArrayList<Estado> Estados) {
        DAL.Estados = Estados;
    }

}
