package Dal.Memento;

import Dal.Indice.IndiceDal;
import java.util.ArrayList;

public class Estado {
    
    private String Entidade;
    private ArrayList<IndiceDal> Indices;

    public ArrayList<IndiceDal> getEstado() {
        return Indices;
    }

    public void setEstado(ArrayList<IndiceDal> Indices, String Entidade) {
        this.Entidade = Entidade;
        this.Indices = Indices;
    }
    
    public boolean isEntidade(String Entidade){
        return (this.Entidade+"").equalsIgnoreCase(Entidade+"");
    }
    
}
