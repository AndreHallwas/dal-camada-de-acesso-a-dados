/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dal.Builder;

import Dal.Consulta.DalConsulta;
import Dal.Indice.IndiceDal;
import Dal.Indice.IndicesDal;
import Dal.Transacao.DalAdicionar;
import Dal.Transacao.DalAlterar;
import Dal.Transacao.DalRemover;
import Dal.Transacao.Decorator.DecAdicionar;
import Dal.Transacao.Decorator.DecAlterar;
import Dal.Transacao.Decorator.DecRemover;
import Dal.Transacao.Transacao;
import Utils.Mensagem;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class DalBuilder {

    private static Object objeto;
    private static String Tabela;
    private static Transacao transacao = null;
    private static boolean FinaldeTransacao = true;

    public static Transacao add() {
        Transacao NovaTransacao;
        if (transacao == null) {
            NovaTransacao = transacao = new DalAdicionar();
        } else {
            NovaTransacao = transacao = new DecAdicionar(transacao);
        }
        NovaTransacao.setTabela(Tabela);
        NovaTransacao.setIndices(null, (IndicesDal) objeto);
        return NovaTransacao;
    }

    public static Transacao alterar() {
        Transacao NovaTransacao;
        if (transacao == null) {
            NovaTransacao = transacao = new DalAlterar();
        } else {
            NovaTransacao = transacao = new DecAlterar(transacao);
        }
        NovaTransacao.setTabela(Tabela);
        NovaTransacao.setIndices(null, (IndicesDal) objeto);
        return NovaTransacao;
    }

    public static Transacao remover() {
        Transacao NovaTransacao;
        if (transacao == null) {
            NovaTransacao = transacao = new DalRemover();
        } else {
            NovaTransacao = transacao = new DecRemover(transacao);
        }
        NovaTransacao.setTabela(Tabela);
        NovaTransacao.setIndices(null, (IndicesDal) objeto);
        return NovaTransacao;
    }

    public static DalConsulta get() {
        DalConsulta consulta = new DalConsulta();
        consulta.setTabela(Tabela);
        consulta.setIndices(null, (IndicesDal) objeto);
        consulta.getIndices();
        consulta.setObjeto(objeto);
        return consulta;
    }

    public static boolean ConcretizarTransacao() {
        setFinaldeTransacao(true);
        return Transacao(true);
    }

    public Object getObjeto() {
        return objeto;
    }

    public static boolean Transacao(boolean Final) {
        if (Final && FinaldeTransacao) {
            boolean flag = transacao == null ? false : transacao.Transacao();
            if (flag == true) {
                transacao = null;
            }else{
                transacao = null;
                Mensagem.ExibirLog("Transacao incompleta");
            }
            return flag;
        } else {
            return true;
        }
    }

    public static void Inicializa(Object objeto, String Tabela) {
        DalBuilder.objeto = objeto;
        DalBuilder.Tabela = Tabela;
    }

    /**
     * @return the FinaldeTransacao
     */
    public static boolean isFinaldeTransacao() {
        return FinaldeTransacao;
    }

    /**
     * @param aFinaldeTransacao the FinaldeTransacao to set
     */
    public static void setFinaldeTransacao(boolean aFinaldeTransacao) {
        FinaldeTransacao = aFinaldeTransacao;
    }

}
